FROM docker/compose:1.13.0
MAINTAINER Antonis Kalipetis <akalipetis@omilia.com>

# Install Docker binary
ADD https://get.docker.com/builds/Linux/x86_64/docker-17.05.0-ce.tgz /docker-17.05.0-ce.tgz
RUN cd / &&\
    tar xzf docker-17.05.0-ce.tgz &&\
    mv docker/* /usr/local/bin/ &&\
    rm -r docker*

ENTRYPOINT []
